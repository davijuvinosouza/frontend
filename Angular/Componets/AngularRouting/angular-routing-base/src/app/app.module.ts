import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path:'mult-table', component: MultTableComponent },
  { path:'mult-table/:id', component: MultTableDetailsComponent },
  { path:'about', component: AboutComponent },
];


import { CommonModule } from '@angular/common';
import { RootComponent } from './root/root.component';
import { HomeComponent } from './home/home.component';
import { MultTableComponent } from './mult-table/mult-table.component';
import { MultTableDetailsComponent } from './mult-table-details/mult-table-details.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AboutComponent } from './about/about.component';



@NgModule({
  declarations: [
    RootComponent,
    HomeComponent,
    MultTableComponent,
    MultTableDetailsComponent,
    NavbarComponent, AboutComponent],
  imports: [
    CommonModule, BrowserModule, RouterModule.forRoot(routes)
  ], 
  bootstrap: [RootComponent],
})
export class AppModule { }
