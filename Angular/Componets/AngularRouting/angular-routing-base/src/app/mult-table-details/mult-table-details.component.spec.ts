import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultTableDetailsComponent } from './mult-table-details.component';

describe('MultTableDetailsComponent', () => {
  let component: MultTableDetailsComponent;
  let fixture: ComponentFixture<MultTableDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MultTableDetailsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MultTableDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
